# www

Ez a weboldal terve.

Repository: https://gitlab.com/segitokutya/www-segitokutya-hu  
Staging: https://segitokutya.onrender.com/  
Site: http://www.segitokutya.hu/  

Jelenlegi sitemap:
* Home
  * Kozhasznusagi jelentes
  * 27/2009 SZMM rendelet
* Hirek
* Rolunk
  * Tortenetunk
  * Terapias helyszineink
* Tagok
  * Bemutatkozasok
* Segito kutyak
  * Mozgasserult segito kutyak
  * Rohamjelzo kutyak
  * Szemelyi segito kutyak
* Terapias kutyak
  * Debrecen
  * Szeged
  * ... (sok)
* Kepzesek
  * Jelentkezesi lapok
  * Terapias kutyakikepzo kepzes OKJ-s
  * Terapias kutya felvezeto
  * Kutyakikepzesek
* Galeria
* Kapcsolat
  * Kapcsolat
  * Linkek
* EFOP Palyazatok

Javasolt sitemap:
* Home
* Hirek
* Rolunk
  * Tortenetunk
  * Terapias helyszineink
  * Tagok
* Kepzesek
* Galeria
* Kapcsolat
* Hasznos
  * 27/2009 SZMM rendelet
  * Linkek
* Jelentesek
  * Kozhasznusagi jelentes
  * EFOP Palyazatok
